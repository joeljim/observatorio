const { timer, of, throwError } = rxjs;
const { 
	tap,
	takeUntil,
	take,
	map,
	combineLatestWith,
	distinctUntilChanged,
	debounceTime,
} = rxjs.operators;



export class PoteService {

	prices = [
		{barcode: '100', name: 'Harina', price: {ved: 12, usd: 2}},
		{barcode: '101', name: 'Arroz', price: {ved: 12, usd: 2}},
		{barcode: '102', name: 'Spagheti', price: {ved: 12, usd: 2}},
		{barcode: '103', name: 'Caraota', price: {ved: 12, usd: 2}},
		{barcode: '104', name: 'Aceite', price: {ved: 12, usd: 2}},
	];


	loggedUser = '';

	configuration = {};

	initialCash = 0;

	departament = '';

	posOpened = true;

	lastInvoiceNumber = 0;
	/** Almacenar factura activa en el pos actualmente */
	activeInvoice = null;

	invoices = [];


    constructor() {

    }
    /** Servicio para verificación
	 *  Sólo emite el valor que le llega
	 */
    echo$(input) {
        return of('echo echo echo:' + input);
    }

	assertOpenPos() {
	
		if(!this.posOpened){
			console.log('Error pos opened');
			return throwError( () => {
				const error = new Error('Debe iniciar sesion');
				error.code = 400;
				return error;
			});		
		}
		return of(null);	
	}

	login$(user, password) {
		if (user === 'peter' && password === '1234') {
			this.loggedUser = user;
			return of(true);
		}
		this.loggedUser = '';
		return of(false);
	}

	/** Establecr los parametros de configuración */
	setup$(configuration) {
		this.configuration = configuration;
		return of(configuration);
	}
	/**
	 * Abre una caja para que comience a facturar
	 *
	 * @param {string} user Usuario que está abriendo la caja
	 * @param {string} password Clave del usuario
	 * @param {number} initialCash Monto de dinero entregado inicialmente
	 * @param {string} branch Nombre de la feria o cooperativa
	 * @param {string} departament Departamento dónde se está abriendo la caja
	 * @returns {boolean} Indica si se abrió satisfactoriamente
	 */
	openPos$(initialCash, departament) {
		// Tengo que validar el usuario esté logueado
		if (this.loggedUser === '') {
			return throwError( () => {
				const error = new Error('Error de usuario');
				error.code = 450;
				return error;
			});
		}
		if (!this.configuration || !this.configuration.branch) {
			return throwError( () => {
				const error = new Error('Caja no ha sido abierta');
				error.code = 440;
				return error;
			}); 
		}
		this.initialCash = initialCash;
		this.departament = departament;
		this.posOpened = true;
		// TODO: Verificar qué deberíamos devolver
		return of(200);
	}

	createInvoice$(cqc) {
		if (!this.posOpened) {
			return throwError( () => {
				const error = new Error('Pos no está abierto');
				error.code = 440;
				return error;
			});
		}
		if (!!this.activeInvoice) {
			return throwError( () => {
				const error = new Error(`Imposible Crear factura con otra abierta, ciérrala o guárdala primero`);
				error.code = 449;
				return error;
			});
		}
		const invoice = {
			id: this.lastInvoiceNumber++,
			cqc: cqc,
			items: []
		}
		/** Guardando factura creada en mi lista interna */
		this.invoices = [invoice].concat(this.invoices);
		/** Colocarla como activa */
		this.activeInvoice = invoice;
		return of(invoice);
	}

	findPrice$(barcode) {

		const result = this.prices.filter(item => item.barcode === barcode);
		if (result.length < 1) {
			return throwError( () => {
				const error = new Error('Producto no encontrado');
				error.code = 480;
				return error;
			});
		}
		if (result.length > 1) {
			return throwError( () => {
				const error = new Error('Varios productos con el mismo código... llama a Angel');
				error.code = 481;
				return error;
			});		
		}
		return of(result[0]);
	}

	addItem$(item) {
		// Procesa el agregar producto
		this.assertOpenPos().subscribe();
		
		if (this.activeInvoice == null) {
			return throwError( () => {
				const error = new Error('No hay factura abierta, por favor, abre una');
				error.code = 485;
				return error;
			});			
		}
		/** Buscar la factura actual */
		const activeInvoice = this.activeInvoice;
		/** Agregamos el producto en la factura actual */
		activeInvoice.items = activeInvoice.items.concat([item]);
		this.activeInvoice = activeInvoice;
		return of(activeInvoice);	
	}

	saveDraftInvoice$() {
		this.assertOpenPos().subscribe();

		if (this.activeInvoice == null) {
			return throwError( () => {
				const error = new Error('No hay factura abierta, por favor, abre una');
				error.code = 485;
				return error;
			});			
		}
		const invoice = {...this.activeInvoice};
		/** Descartamos la factura de activa */
		this.activeInvoice = null;
		return of(invoice);
	}

	recoverDraftInvoice$(invoiceId) {
		console.log('invoiceid:', invoiceId);
		this.assertOpenPos().subscribe();

		if (this.activeInvoice !== null) {
			return throwError( () => {
				const error = new Error('Ya hay una factura activa, ciérrala o guárdala');
				error.code = 485;
				return error;
			});			
		}
		const filtered = this.invoices.filter( invoice => invoice.id === invoiceId);
		
		console.log('001 Que hay en filter:', filtered);
		console.log('002 Que hay en invoices:', this.invoices);
		
		if (filtered.length !== 1) {
			return throwError( () => {
				const error = new Error('No sé que te fumaste, pero esa factura no existe');
				error.code = 459;
				return error;
			});
		}
		this.activeInvoice = filtered[0];
		return of(filtered[0]);
	}

	 //que recibe la funcion removeItem(posicion) cual es el contrato?
	 removeItem$() {
	 	//validar de que el pos este abierto
	 	this.assertOpenPos().subscribe();
	 	//validar de que sea factura actual
	 	if (this.activeInvoice == null) {
	 		return throwError(() => {
	 			const error = new Error("sin items que eliminar");
	 			return error;
	 		});
	 	}
	 	//buscar la factura actual
	 	const activeInvoice = this.activeInvoice;
	 	const price = activeInvoice.items;
	 	//eliminar item en la factura acutal
	 	if (price.length >= 1) {
	 		price.pop();
	 	}
	 	if (price.length == 0) {
	 		return throwError(() => {
	 			const error = new Error("sin items que eliminar");
	 			return error;
	 		});
	 	}

	 	return of(activeInvoice);
	 }
	
	  /**
	   * variables: paymentEntered
	   * 			invoiceAmount
	   * -----funcion validar pago---
		* validar open pos
	   * validar factura actual
	   * ir a factura actual
	   * condicion: si pagoIngresado == montoFactura ? imprimir=guardar
	   * si pagoIntroducido < montofactura ? cancelar diferencia: ir  atras
	   * si pagoIntroducido > montofactura ? dar vuelto  :  ir atras
	   */
	 validatePayment$(invoiceAmount,paymentEntered){
		//TODO: validar openPOs
		//TODO: validar factura actual
		const invoice= this.activeInvoice;
		console.log("invoice en validatePayment",invoice);
		let alert= ""
		if(invoiceAmount === paymentEntered){
			//TODO: funcion cerrar factura
			return of("pago completado")
		}else if(paymentEntered < invoiceAmount){
			of("cancelar diferencia");
		}else if(paymentEntered > invoiceAmount){
			of("su vuelto es ...");
		}

	 }




	  /*------ funcion cerrar factura---
	  	*guardar factura sin posibilidad a modificar
		* imprimir factura 
		*/
	 	/**
		  * --limpiar factura 
		  * limpiar cqc
		  * limpiar arreglo de items
		  * actualizar monto a 0
		  */
	 //TODO:contratos?
		 cleanInvoice$(){
			 //validar openPos
			 //validar que sea la factura actual 
			 //obtener factura actual 
			 const invoice= this.activeInvoice;
			 const prices = invoice.items;
			 prices.splice(0);
			 console.log("item cleanInvoice",prices);
			 console.log("invoice en clean invoice",invoice);
		 }





	  /**funciones
	   * obtener subtotal
	   * limpiar pantalla 
	   * editar cantidad
	   * 
	   */
	closeInvoice() {
		//validar openPos
		this.assertOpenPos().subscribe();
		//validar de que sea factura actual
		if (this.activeInvoice == null) {
			return throwError(() => {
				const error = new Error("sin items que eliminar");
				return error;
			});
		}
	}		







}